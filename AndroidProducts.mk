PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_A37.mk \
    $(LOCAL_DIR)/lineage_A37_gms_go.mk

# Lunch choices
COMMON_LUNCH_CHOICES := \
    lineage_A37-eng \
    lineage_A37-userdebug \
    lineage_A37-user \
    lineage_A37_gms_go-userdebug \
    lineage_A37_gms_go-user
